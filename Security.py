#**********************************************************
# Project: Security System
# Programmer: William Adams
# Purpose: Detect and track security threats
# Copywrite 2015, March 10, William Adams
#  all rights reserved, obtain permission to use code
#**********************************************************


import cv2
import numpy as np
from scipy import sparse
from scipy import optimize
import threading as td #included here for future threading


#This class is used to represent the model for a stacked sparse autoencoder
# with softmax output. Initially it expects a 30x30 window flattened as the
# input, 1000 nodes in the hidden layer, and 100 nodes in the output layer.
class Model:
   #This class represents the model itself, complete with training
   # functions for different modes of operation

   def __init__(self, *args):
       if args is ():
           #for use if calling  m = Model()
           self.S1 = 900 #expects a 30x30 window with 1 spectral value
           self.S2 = 1000 #size of the hidden layer
           self.S3 = 100 #size of softmax layer
           #generate weights and normalize along inputs to next layer
           self.W1 = np.random.rand(self.S2,self.S1)
           self.W1 = self.W1 / np.sum(self.W1,axis=1)[:,np.newaxis]
           self.W2 = np.random.rand(self.S3,self.S2)
           self.W2 = self.W2 / np.sum(self.W2,axis=1)[:,np.newaxis]
           self.b1 = np.zeros((self.S2,1))
           #generates weights and bias for autoencoder
           # keeping track of this will better train hidden layer
           self.Wae = np.random.rand(self.S1,self.S2)
           self.Wae = self.Wae / np.sum(self.Wae,axis=1)[:,np.newaxis]
           self.bae = np.zeros((self.S1,1))
           #fine-tuning parameters
           self.lam = 1e-5
           self.beta = 1e-5
           self.sparsityparam = 1e-5
       else:
           #for use if calling  n = Model(m)
           self.S1 = args[0].S1
           self.S2 = args[0].S2
           self.S3 = args[0].S3
           self.W1 = args[0].W1
           self.W2 = args[0].W2
           self.b1 = args[0].b1
           self.Wae = args[0].Wae
           self.bae = args[0].bae
           self.lam = args[0].lam
           self.beta = args[0].beta


   #Regresses the batch of samples, returning probabilities of the label
   # being in each class colum-wise for each sample row-wise
   def regress(self, batch):
       #regress, producing labels for the batch of data input
       a2 = self.computefeatures(batch)
       a3 = self.__sigm(np.dot(a2,self.W2.T))
       e = np.exp(a3)
       return e / np.sum(e,axis=1)[:,np.newaxis]


   #Classifies the samples producing labels
   def classify(self,batch):
       a2 = self.computefeatures(batch)
       a3 = self.__sigm(np.dot(a2,self.W2.T))
       return np.argmax(a3,axis=1)


   #Trains the model given the batch of samples and labels
   # when labels are present, the softmax regressor is trained,
   # when labels are not present, the autoencoder is trained as well
   def train(self, batch, labels=[]):
       if labels == []: #if labels is empty, produce some labels          
           labels = np.argmax(self.regress(batch),axis=1)
           x0 = np.concatenate((self.W1.flatten(),self.Wae.flatten(),
                               self.b1.flatten(),self.bae.flatten()))
           res = optimize.minimize(self.__AECost, x0,
                                  args = (batch,), method = 'L-BFGS-B',
                                  jac = True, options = {'maxiter': 400})
           #unpack the new weights
           opt_theta = res.x
           self.W1 = opt_theta[0:self.S1*self.S2].reshape(self.W1.shape)
           self.Wae = opt_theta[self.S1*self.S2:self.S1*self.S2*2].reshape(self.Wae.shape)
           self.b1 = opt_theta[self.S1*self.S2*2:self.S1*self.S2*2+self.S2].reshape(self.b1.shape)
           self.bae = opt_theta[len(opt_theta)-self.S1:len(opt_theta)].reshape(self.bae.shape)

       #now train the softmax classifier with labels
       x0 = self.W2.flatten()
       res = optimize.minimize(self.__SoftmaxCost, x0,
                                args = (batch,labels), method = 'L-BFGS-B',
                                jac = True, options = {'maxister': 400})
       opt_theta = res.x
       self.W2 = opt_theta.reshape(self.W2.shape)
        

   #Computes the features in a feed-foward pass through to the last hidden layer
   def computefeatures(self, batch):
       return self.__sigm(np.dot(batch,self.W1.T)+self.b1.T)


   #includes only the autoencoder cost and gradient for weights W1
   #This part is really slow and needs optimized even though everything is vectorized
   def __AECost(self, theta, batch):

       #unpack values of theta
       W1 = theta[0:self.S1*self.S2].reshape(self.W1.shape)
       Wae = theta[self.S1*self.S2:self.S1*self.S2*2].reshape(self.Wae.shape)
       b1 = theta[self.S1*self.S2*2:self.S1*self.S2*2+self.S2].reshape(self.b1.shape)
       bae = theta[len(theta)-self.S1:len(theta)].reshape(self.bae.shape)
      
       #feed forward pass
       a2 = self.__sigm(np.dot(batch,W1.T)+b1.T)
       a3 = self.__sigm(np.dot(a2,Wae.T)+bae.T)
       C = a3-batch
       #feed back pass
       d3 = C*self.__sigmGrad(a3)
       baegrad = np.sum(d3,axis=0)/d3.shape[0]
       cost = np.trace(np.dot(C.T,C))/C.shape[0]
       d2 = np.dot(d3,Wae)*self.__sigmGrad(a2)
       b1grad = np.sum(d2,axis=0)/d2.shape[0]
       Waegrad = np.dot(d3.T,a2)/d3.shape[0] + self.lam*Wae
       W1grad = np.dot(d2.T,batch)/d2.shape[0] + self.lam*W1
       cost = cost + (self.lam/2.0)*np.trace(np.dot(W1.T,W1))
       cost = cost + (self.lam/2.0)*np.trace(np.dot(Wae,Wae.T))
       #also include sparsity constraint
       ph2 = np.sum(a2,axis=0)/a2.shape[0]
       act2 = self.beta*((1-self.sparsityparam)/(1-ph2) - (self.sparsityparam/ph2))
       cost = cost + self.beta*np.sum(self.sparsityparam*np.log(self.sparsityparam/ph2)+
                                (1-self.sparsityparam)*np.log((1-self.sparsityparam)/(1-ph2)))
       grad = np.concatenate((W1grad.flatten(),Waegrad.flatten(),
                             b1grad.flatten(),baegrad.flatten()))
       return [cost, grad]

    
   #Used for training the softmax output layer weights W2
   def __SoftmaxCost(self, theta, batch, labels):
       #unpack theta values
       W2 = theta.reshape(self.W2.shape)
       #form ground-truth matrix
       gt = np.array(labels == np.arange(self.S3)[:,np.newaxis]).T
       #compute probabilities on each class
       a2 = self.computefeatures(batch)
       a3 = self.__sigm(np.dot(a2,self.W2.T))
       e = np.exp(a3)
       e = e - np.max(e,axis=1)[:,np.newaxis]
       e = e / np.sum(e,axis=1)[:,np.newaxis]
       #compute cost
       cost = (self.lam/2.0)*np.dot(theta,theta.T)
       #in this line, +1e-8 is added in the log to avoide taking log(0)
       cost = cost - np.sum(np.log(np.take(e,labels)+1e-8))/e.shape[0]
       #compute gradient
       grad = self.lam*theta - np.dot(a2.T,gt-e).T.flatten()
       return [cost, grad]

   #sigma function used in the AECost function
   def __sigm(self,z):
       return 1/(1+np.exp(-1*np.array(z)))

   #gradient of the sigma function used in the AECost function
   def __sigmGrad(self,z):
       z = np.array(z)
       return z*(1-z)



if __name__ == '__main__':

    #I've included notes to indicate where the program is but
    # I'll try to turn this into a visual representation.
    print("Starting")

    winname = 'Window1'

    #Initialize video feed from camera 0
    vid = cv2.VideoCapture(0)
    img = vid.read()
    sz = img[1].shape

    #Initialize model
    K = 100 #number of clusters
    missratio = 0.03 #number of grid sections allowed to be wrong
    win = [30,30,1] #window size
    m = Model()
    print("Built Model")
    # Initially I wanted to train on 10,000 samples but it takes too long
    Nu = 1000 #initial number of unsupervised training examples.
    gs = [16,16] #grid stepsize
    #sz=(480,640,3), prime factors of
    # 480 are 2^5 * 3 * 5
    # 640 are 2^7 * 5

    #form a grid for detection
    grid = np.meshgrid(np.arange(gs[0],sz[0],gs[0]),
                       np.arange(gs[1],sz[1],gs[1]))
    grid = np.array([grid[0].flatten(),grid[1].flatten()]).T
    Ns = grid.shape[0] #number of grid points
    labels = []
    prevlabels = []
    
    print("Starting Loop")
    cv2.namedWindow(winname)
    while(img[0]):
        I = cv2.cvtColor(img[1],cv2.COLOR_BGR2GRAY) #convert to greyscale
        B = cv2.bilateralFilter(I,5,100,100) #bilateral filter
        
        #UNSUPERVISED LEARNING
        # -I plan to thread this part in the future
         #produce indices within the appropriate range as patch centers
        idx = np.round(np.random.rand(Nu,2)*[sz[0]-win[0],sz[1]-win[1]])
        idx = idx.astype(np.uint16) + [win[0]/2,win[1]/2]
         #take random patches from image called batch
        batch = np.zeros((Nu,win[0]*win[1]*win[2]), B.dtype)
        for i in range(Nu):
            batch[i] = B[idx[i,0]-win[0]/2:idx[i,0]+win[0]/2,
                         idx[i,1]-win[1]/2:idx[i,1]+win[1]/2].flatten()
        if Nu>50: #reset Nu after initial batch to save time
            Nu=50 #If this is too slow, change condition and result
        print("Started Unsupervised Learning")
        m.train(batch) #unsupervised learning
        print("Finished Unsupervised Learning")


        #SUPERVISED LEARNING
        # take grid of patches from image called batch
        batch = np.zeros((Ns,win[0]*win[1]*win[2]), B.dtype)
        for i in range(Ns):
            batch[i] = B[grid[i,0]-win[0]/2:grid[i,0]+win[0]/2,
                         grid[i,1]-win[1]/2:grid[i,1]+win[1]/2].flatten()
        # determine labels only during first iteration of loop
        if labels==[]:
            #In this section, I perform k-means on the features to produce
            # labels at each point in the detection grid but I may change
            # this to assign individual labels to each grid point instead.
            #transform the data into features
            Feats = m.computefeatures(batch)
            print("Starting K-Means")
            #input to k-means with K=100 or so classes
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
            flags = cv2.KMEANS_RANDOM_CENTERS
            labels = cv2.kmeans(Feats.astype(np.float32),K,criteria,10,flags)[1]
            labels = labels.reshape((labels.shape[0],))
            print("Finished K-Means")
            #these labels are in order corrosponding to points in batch with values
            # on the integers from 0-99
            
            #train softmax regressor on these labels
            print("Starting Initial Supervised Learning")
            m.train(batch, labels) #initial supervised learning
            print("Finished Initial Supervides Learning")
            
        else:
            #if preveious labels is empty, fill it and move on to third iteration
            if prevlabels==[]:
                prevlabels = labels.copy()
                continue
            #round labels for regression
            prevlabels = labels.copy()
            labels = m.classify(batch)
            misses = prevlabels!=labels
            if np.sum(misses,dtype=np.float16)/labels.shape[0] > missratio:
                print("ALARM: Miss Ratio Exceeded")
                #here thread off into some action to be executed

            #supervised learning
            m.train( np.delete(batch,misses.T,0), np.delete(labels,misses.T,0) )
            

        #here I should draw boxes on the detection areas in green to
        # indicate good detection and red to indicate bad detection
        cv2.imshow(winname,B) #display filtered image
        if cv2.waitKey(1)==27: #hit escape key to close window
            break
        img = vid.read() #read another frame
        
    cv2.destroyAllWindows() #close/destroy all windows

