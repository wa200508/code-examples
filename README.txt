Author: William Adams
Date: 2/9/2015

As you may have noticed, this fold is still
under construction. I'm compiling not just any
random code but working to complete the commenting
and styling of projects I've tackled in school.
As soon as these are nicely polished I will have
them up and ready for display. If you're interested
in the rough versions feel free to let me know on
my website:

www.dataisbeautiful.weebly.com