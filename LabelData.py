#**********************************************************
# Project: Road Tracking
# Subroutine: Label Data
# Programmer: William Adams
# Purpose: Label road markings for supervised learning
# How To: This program opens the Samples folder,
#  selects a video from which to extract samples
#  of line segments, and allows a user to generate
#  subwindow samples. A left click on the image will
#  capture the w by w subwindow around the sample
#  shown as a red box and store it in the location
#  defined by samplespath. The right click moves
#  to the next fs-th frame in the sequence until the
#  end of the video is reached. At any time the user
#  may press the escape key (Esc) to end the program
#  or simply right click until the end of the video
#  is reached. 
#**********************************************************

import cv2
import cv2.cv as cv
import numpy as np
from os import listdir
from os.path import isfile, join

pos = [-1,-1]
events = [False,False]

def on_mouse(event, x, y, flags, params):
    global pos, events
    pos = [x,y]
    if event == cv.CV_EVENT_LBUTTONDOWN:
        events[0] = True
    elif event == cv.CV_EVENT_RBUTTONDOWN:
        events[1] = True
        

if __name__ == '__main__':

    vidname = 'Glare-Middle'
    vid = cv2.VideoCapture('Samples/Robust/'+vidname+'.wmv')
    img = vid.read()
    sz = img[1].shape
    w = 30 #window size
    fs = 5 #frame step size

    winname = 'Win1'
    cv2.namedWindow(winname)
    cv.SetMouseCallback(winname, on_mouse, 0)

    #append to the file by finding the last index in the folder
    index = 0
    samplespath = 'Samples/Roadline Samples'
    pre = 'f'
    post = '.jpg'
    for f in listdir(samplespath):
        if isfile(join(samplespath,f)):
            index = max([index,int(f.replace(pre,'').replace(post,''))])
    index = index + 1

    cont = True
    while(img[0] and cont):
        while(cont):
            X = img[1].copy()

            #determine if selected window fits in the frame
            inframe = pos[0]>w/2 and pos[0]<(sz[1]-w/2)
            inframe = inframe and pos[1]>w/2 and pos[1]<(sz[0]-w/2)
                
            if inframe:
                cv2.rectangle(X,(pos[0]-w/2,pos[1]-w/2),(pos[0]+w/2,pos[1]+w/2),(0,0,255))

            cv2.imshow(winname,X)
            if cv2.waitKey(1)==27:
                cont = False
                
            if events[0] and inframe:
                print('Saving Image')
                win = img[1][pos[1]-w/2:pos[1]+w/2,pos[0]-w/2:pos[0]+w/2]
                cv2.imwrite(samplespath+'/'+pre+str(index)+post,win)
                index = index + 1
                events[0] = False

            if events[1]:
                events[1] = False
                break

        #step through video fs frames at a time
        for i in range(fs):
            img = vid.read()
            if img[0]==False:
                cont = False
                break
        
    cv2.destroyAllWindows()
            
